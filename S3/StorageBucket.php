<?php

namespace yii\storage\S3;

use Aws\S3\S3Client;
use yii\base\InvalidCallException;
use yii\storage\BaseStorageBucket;
use yii\storage\StorageBucketInterface;

/**
 * Class StorageBucket
 * @package yii\storage\S3
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 0.1
 */
class StorageBucket extends BaseStorageBucket implements StorageBucketInterface
{
    const ACL_PRIVATE     = 'private';
    const ACL_PUBLIC_READ = 'public-read';
    /**
     * @var S3Client
     */
    protected $s3Client;

    /**
     * @param S3Client $client
     */
    public function setS3Client(S3Client $client)
    {
        if($this->s3Client instanceof S3Client){
            throw new InvalidCallException('S3Client client is already set');
        }
        $this->s3Client = $client;
    }

    /**
     * @inheritdoc
     */
    public function put($source, $dest)
    {
        $result = $this->s3Client->upload(
            $this->bucketName,
            $dest,
            self::ACL_PUBLIC_READ
        );

        return $result->offsetGet('ObjectURL') ?: false;
    }

    /**
     * @inheritdoc
     */
    public function file($filename)
    {
        return new StorageFile([
            'bucket'   => $this,
            'filename' => $filename
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getObjectUrl($filename)
    {
        return $this->s3Client->getObjectUrl($this->bucketName, $filename);
    }
}