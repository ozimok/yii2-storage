<?php

namespace yii\storage\S3;

use Aws\S3\S3Client;
use yii\storage\BaseStorage;
use yii\storage\StorageInterface;

/**
 * Class Storage
 * @package yii\storage\S3
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 0.1
 */
class Storage extends BaseStorage implements StorageInterface
{
    const CLIENT_VERSION  = 'latest';
    /**
     * @var string Amazon access key
     */
    public $accessKey;
    /**
     * @var string Amazon secret access key
     */
    public $secretAccessKey;
    /**
     * @var string
     */
    public $signatureVersion = 'v4';
    /**
     * @var string
     */
    public $region = 'eu-central-1';
    /**
     * @inheritdoc
     */
    public $basePath = 's3://';
    /**
     * @var S3Client
     */
    protected $s3Client;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->s3Client = new S3Client([
            'credentials' => [
                'key'    => $this->accessKey,
                'secret' => $this->secretAccessKey
            ],
            'region'  => $this->region,
            'version' => self::CLIENT_VERSION
        ]);

        $this->s3Client->registerStreamWrapper();
    }

    /**
     * @inheritdoc
     */
    public function bucket($bucketName = null)
    {
        return new StorageBucket([
            's3Client'    => $this->s3Client,
            'bucketName'  => $bucketName ?: $this->defaultBucket,
            'path2bucket' => $this->basePath
        ]);
    }
}