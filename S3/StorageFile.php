<?php

namespace yii\storage\S3;

use yii\storage\BaseStorageFile;
use yii\storage\StorageFileInterface;

/**
 * Class StorageFile
 * @package yii\storage\S3
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 0.1
 */
class StorageFile extends BaseStorageFile implements StorageFileInterface
{
    /**
     * @inheritdoc
     */
    public function getUrl()
    {
        return $this->bucket->getObjectUrl($this->filename);
    }
}