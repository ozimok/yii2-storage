<?php

namespace yii\storage;

use \yii\base\Object;

/**
 * Class BaseStorageFile
 * @package yii\storage
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 1.0
 */
class BaseStorageFile extends Object
{
    /**
     * @var StorageBucketInterface
     */
    public $bucket;
    /**
     * @var string
     */
    public $filename;
    /**
     * @var string
     */
    protected $fileFullPath;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->fileFullPath = $this->bucket->normalizePath($this->filename);
    }

    /**
     * @inheritdoc
     */
    public function copy($dest)
    {
        return copy($this->fileFullPath, $this->bucket->normalizePath($dest));
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        return unlink($this->fileFullPath);
    }

    /**
     * @inheritdoc
     */
    public function exists()
    {
        return file_exists($this->fileFullPath);
    }

    /**
     * @inheritdoc
     */
    public function filesize()
    {
        return filesize($this->fileFullPath);
    }
}