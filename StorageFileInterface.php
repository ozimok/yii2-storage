<?php

namespace yii\storage;

/**
 * Interface StorageFileInterface
 * @package yii\storage
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 1.0
 */
interface StorageFileInterface
{
    /**
     * @param string $dest
     * @return bool
     */
    public function copy($dest);

    /**
     * Delete file
     *
     * @return bool
     */
    public function delete();

    /**
     * Check is file exists
     *
     * @return bool
     */
    public function exists();

    /**
     * @return integer|false
     */
    public function filesize();

    /**
     * @return string|false
     */
    public function getUrl();
}