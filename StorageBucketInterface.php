<?php

namespace yii\storage;

/**
 * Interface StorageBucketInterface
 * @package yii\storage
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 1.0
 */
interface StorageBucketInterface
{
    /**
     * @param string $source
     * @param string $dest
     * @return mixed
     */
    public function put($source, $dest);

    /**
     * @param string $filename
     * @return StorageFileInterface
     */
    public function file($filename);

    /**
     * @param string $filename
     * @return string
     */
    public function normalizePath($filename);

    /**
     * @param string $filename
     * @return string
     */
    public function getObjectUrl($filename);
}