<?php

namespace yii\storage;

use \yii\base\Object;
use yii\base\InvalidConfigException;

/**
 * Class BaseStorageBucket
 * @package yii\storage
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 0.1
 */
class BaseStorageBucket extends Object
{
    /**
     * @var string
     */
    public $bucketName;
    /**
     * @var string
     */
    public $path2bucket;
    /**
     * @var string
     */
    protected $bucketPath;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->bucketName)) {
            throw new InvalidConfigException("The 'bucketName' property must be specified.");
        }
        $this->bucketPath = $this->path2bucket . $this->bucketName . DIRECTORY_SEPARATOR;
    }

    /**
     * @inheritdoc
     */
    public function normalizePath($filename)
    {
        return $this->bucketPath . $filename . DIRECTORY_SEPARATOR;
    }
}