<?php

namespace yii\storage;

use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class BaseStorage
 * @package yii\storage
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 1.0
 */
class BaseStorage extends Component
{
    /**
     * @var string
     */
    public $defaultBucket;
    /**
     * @var string
     */
    protected $basePath;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->defaultBucket)) {
            throw new InvalidConfigException("The 'defaultBucket' property must be specified.");
        }
    }

    /**
     * @inheritdoc
     */
    public function createBucket($bucketName)
    {
        return mkdir($this->basePath . $bucketName);
    }

    /**
     * @inheritdoc
     */
    public function deleteBucket($bucketName)
    {
        return rmdir($this->basePath . $bucketName);
    }
}