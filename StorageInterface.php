<?php

namespace yii\storage;

/**
 * Interface StorageInterface
 * @package yii\storage
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 1.0
 */
interface StorageInterface
{
    /**
     * @param string|null $bucketName
     * @return StorageBucketInterface
     */
    public function bucket($bucketName = null);

    /**
     * @param string $bucketName
     * @return string mixed
     */
    public function createBucket($bucketName);

    /**
     * @param $bucketName
     * @return mixed
     */
    public function deleteBucket($bucketName);
}