<?php

namespace yii\storage;

use Yii;
use yii\db\BaseActiveRecord;
use yii\base\Behavior;
use yii\web\UploadedFile;
use yii\validators\FileValidator;

/**
 * Class FileUploadBehavior
 * @package yii\storage\behaviors
 * @author Oleg Ozimok <ozimok.oleg@gmail.com>
 * @since 0.1
 *
 * @property \yii\db\BaseActiveRecord $owner
 */
class FileUploadBehavior extends Behavior
{
    /**
     * @var string
     */
    public $uploadedFileAttribute = 'file';
	/**
	 * @var string (use default if empty)
	 */
    public $storageBucketName;
    /**
     * ~~~
     * [
     *     'size'      => 'mySize',
     *     'mime_type' => 'myMimeType'
     * ]
     * ~~~
     *
     * @var array
     */
	public $additionalAttributes = [];
    /**
     * @var array
     */
    public $validationRules = [];
    /**
     * @var string
     */
    public $fileSaveErrorMessage;
    /**
     * @var string
     */
    public $fileRemoveErrorMessage;
    /**
     * @var UploadedFile|null
     */
    protected $uploadedFile;
    /**
     * @var \yii\storage\StorageBucketInterface
     */
    protected $storageBucket;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->storageBucket = Yii::$app->storage->bucket($this->storageBucketName);

        if(empty($this->fileSaveErrorMessage)){
            $this->fileSaveErrorMessage = Yii::t('app', 'Unable to save file');
        }
        if(empty($this->fileRemoveErrorMessage)){
            $this->fileRemoveErrorMessage = Yii::t('app', 'Unable to remove file');
        }
    }

	/**
	 * @inheritdoc
	 */
	public function attach($owner)
	{
		parent::attach($owner);

		if(!($this->owner->getAttribute($this->uploadedFileAttribute) instanceof UploadedFile)){
			$this->uploadedFile = UploadedFile::getInstance($this->owner, $this->uploadedFileAttribute);
		}
	}

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_VALIDATE  => 'validateFile',
            BaseActiveRecord::EVENT_BEFORE_INSERT   => 'saveFile',
            BaseActiveRecord::EVENT_BEFORE_DELETE   => 'removeFile',
        ];
    }

    /**
     * Validate file
     */
    public function validateFile()
    {
        $validator = new FileValidator($this->validationRules);
        if (!$validator->validate($this->uploadedFile, $error)) {
            $this->owner->addError($this->uploadedFileAttribute, $error);
        }
    }

    /**
     * Save file and set filename to model attribute
     */
    public function saveFile()
    {
        if(($filename = $this->storageBucket->put($this->uploadedFile->tempName, 'uhhuhu'))){
            $this->removeTempFile();
            $this->setUploadedFileAttribute($filename);
	        $this->setAdditionalAttributes();
        } else {
            $this->owner->addError($this->uploadedFileAttribute, $this->fileSaveErrorMessage);
        }
    }

    /**
     * @return bool
     */
    public function removeTempFile()
    {
        return unlink($this->uploadedFile->tempName);
    }

	/**
	 * Set owner file attribute
	 * @param string $value
	 */
	public function setUploadedFileAttribute($value)
	{
		$this->owner->setAttribute($this->uploadedFileAttribute, $value);
	}

	/**
	 * Set owner additional attributes
	 */
	public function setAdditionalAttributes()
	{
		if(!empty($this->additionalAttributes)){
			foreach($this->additionalAttributes as $uploadedFileAttribute => $ownerAttribute){
				$this->owner->setAttribute($ownerAttribute, $this->uploadedFile->{$uploadedFileAttribute});
			}
		}
	}

    /**
     * Remove file
     */
    public function removeFile()
    {
        $filename = $this->owner->getAttribute($this->uploadedFileAttribute);
	    if(!$this->storageBucket->file($filename)->delete()){
            $this->owner->addError($this->uploadedFileAttribute, $this->fileRemoveErrorMessage);
        }
    }
}